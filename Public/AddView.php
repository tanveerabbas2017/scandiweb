<?php

include_once '../Classes/typehandler.php';

$Object = new ProductController;
 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <link rel="preconnect" href="https://fonts.gstatic.com">
   
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Zilla+Slab+Highlight&display=swap" rel="stylesheet">   
    <link rel="stylesheet" href="../src/main.css">
    
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>


</head>
<body>


<div class="content">
<header>

<div class="header">

<h1 class="scandi" >PRODUCT ADD</h1> 

<a href="../Public/ProductView.php"><button class="btn-cancle">Cancle</button></a>

    
</div>

<hr>
</header>

 <main>
<div class="form"  id="form">

    <form action="AddView.php" id="my_form"  method="Post"  onsubmit=" return validate();"    >

        <label for="" class="form-label">Sku</label>
           <input  class="form-control" type="text"  id="sku"  name="sku" >
          
           <span class="error" id="sku_error">
           
           <?php echo $Object->product_Value(); ?>
           </span><br>
           
           <label for="">Name</label>
           <input class="form-control" type="text" id="name"   name="name" >
           <span class="error" id="name_error"></span>
           <br>
           
           
           <label for="">Price($)</label>
           <input class="form-control" type="text" id="price"  name="price" >
           <span class="error" id="price_error"></span>
          <br>


           <label for="">Type</label>
           
           <select class="form-control" name="type" id="type">
           
               <option value="disk">Disk</option>
               <option value="book">Book</option>
               <option value="furniture">Furniture</option>
           </select>
           <br>

           <div class="size" id="size">
           <label for="">Size(MB)</label>
           <input class="form-control" type="text"  id="sizei"  name="size"  ><span class="error" id="size_error" ></span>
           <br>
           <p class="description" >“Please, provide size”</p>
           
           </div>


           <div class="weight" id="weight">
           <label for="">Weight(KG)</label>
           <input class="form-control" type="text" id="weighti"  name="weight"  >
           <span class="error" id="weight_error"></span>
          <br>
           <p class="description">“Please, provide weight”</p>
           </div>



           <div class="dimension"  id="dimension">

           <label for="">width(CM)</label>
           <input class="form-control" type="text" id="width" name="width">
           <span class="error" id="width_error"></span>
          <br>


           <label for="">Length(CM)</label>
           <input class="form-control" type="text"  id="length" name="length">
           <span class="error" id="length_error"></span>
           <br>


           <label for="">Height(CM)</label>
           <input class="form-control" type="text" id="height"  name="height">
           <span class="error" id="height_error"></span>
           <br>
           <p class="description">“Please, provide dimensions”</p>
           </div>
           



    <input  type="submit" id="submit" value="Save"   class="submit" name="submit"> 
 
    


    </form>


    </div> 
    </main>

    <footer>

    <div class="footer">

        <hr>

        <p>Scandiweb Test Assignment</p>

    </div>

    </div>
    </footer> 














   



    </script>

<script src="../js/main.js"></script>



</body>
</html>